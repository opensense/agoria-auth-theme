<#import "template.ftl" as layout>
<@layout.registrationLayout; section>
    <#if section = "title">
       ${msg("loginTitle",realm.displayName)?no_esc}
    <#elseif section = "header">
        ${msg("loginTitleHtml",realm.displayNameHtml)?no_esc}
    <#elseif section = "form">
        <form id="kc-totp-login-form" class="form totp ${properties.kcFormClass!}" action="${url.loginAction}" method="post">
            <div class="${properties.kcFormGroupClass!}">
                <#--  <div class="${properties.kcLabelWrapperClass!}">
                    <label for="totp" class="${properties.kcLabelClass!}">${msg("loginTotpOneTime")}</label>
                </div>

                <div class="${properties.kcInputWrapperClass!}">
                    <input id="totp" name="totp" autocomplete="off" type="text" class="form-control ${properties.kcInputClass!}" autofocus />
                </div>  -->

                <div class="mdc-text-field mdc-text-field--outlined mdc-text-field--with-leading-icon ${properties.kcLabelClass!} <#if usernameEditDisabled??>mdc-text-field--disabled</#if>">
                    <i class="material-icons mdc-text-field__icon" tabindex="-1" role="button">lock</i>
                    <input id="totp" name="totp" type="text" autofocus autocomplete="off" class="form-control ${properties.kcInputClass!}">
                    <div class="mdc-notched-outline">
                        <div class="mdc-notched-outline__leading"></div>
                        <div class="mdc-notched-outline__notch">
                        <label for="totp" class="mdc-floating-label ${properties.kcLabelClass!}">
                            ${msg("loginTotpOneTime")}
                        </label>
                        </div>
                        <div class="mdc-notched-outline__trailing"></div>
                    </div>
                </div>


            </div>

            <div class="${properties.kcFormGroupClass!} totp-button-container">
                <div id="kc-form-options" class="${properties.kcFormOptionsClass!}">
                    <div class="${properties.kcFormOptionsWrapperClass!}">
                    </div>
                </div>

                <div id="kc-form-buttons" class="${properties.kcFormButtonsClass!}">
                    <div class="${properties.kcFormButtonsWrapperClass!} row">
                        <div class="col-xs-6 col-xs-push-6 col-sm-8 col-sm-push-4">
                            <button class="${properties.kcButtonClass!} ${properties.kcButtonPrimaryClass!} ${properties.kcButtonLargeClass!}" name="login" id="kc-login" type="submit">
                                ${msg("doLogIn")?no_esc}
                            </button>
                        </div>
                        <div class="col-xs-6 col-xs-pull-6 col-sm-4 col-sm-pull-8">
                            <button type="submit"  class="${properties.kcButtonClass!}" name="cancel" id="kc-cancel" > 
                            <#--  onclick="redirect(event, '${url.loginUrl}')"   -->
                                <i class="material-icons mdc-button__icon" aria-hidden="true">arrow_back</i>
                                ${msg("doCancel")?no_esc}
                            </button>
                            <#--  <input class="${properties.kcButtonClass!} ${properties.kcButtonDefaultClass!} ${properties.kcButtonLargeClass!}" name="cancel" id="kc-cancel" type="submit" value="${msg("doCancel")}"/>  -->
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </#if>
</@layout.registrationLayout>
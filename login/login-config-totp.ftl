<#import "template.ftl" as layout>
<@layout.registrationLayout displayInfo=true; section>
    <#if section = "title">
        ${msg("loginTotpTitle")?no_esc}
    <#elseif section = "header">
        ${msg("loginTotpTitle")?no_esc}
    <#elseif section = "form">
    <ol id="kc-totp-settings">
        <li>
            <p class="mdc-typography--body1">${msg("loginTotpStep1")?no_esc}</p>
            <ul id="kc-totp-supported-apps">
                <#list totp.policy.supportedApplications as app>
                    <li class="mdc-typography--body1">${app}</li>
                </#list>
            </ul>
        </li>

        <#if mode?? && mode = "manual">
            <li>
                <p class="mdc-typography--body1">${msg("loginTotpManualStep2")}</p>
                <p><span id="kc-totp-secret-key">${totp.totpSecretEncoded}</span></p>
                <p><a href="${totp.qrUrl}" id="mode-barcode" class="mdc-typography--body1">${msg("loginTotpScanBarcode")}</a></p>
            </li>
            <li>
                <p class="mdc-typography--body1">${msg("loginTotpManualStep3")}</p>
                <p>
                    <ul>
                        <li id="kc-totp-type" class="mdc-typography--body1">${msg("loginTotpType")}: ${msg("loginTotp." + totp.policy.type)}</li>
                        <li id="kc-totp-algorithm" class="mdc-typography--body1">${msg("loginTotpAlgorithm")}: ${totp.policy.getAlgorithmKey()}</li>
                        <li id="kc-totp-digits" class="mdc-typography--body1">${msg("loginTotpDigits")}: ${totp.policy.digits}</li>
                        <#if totp.policy.type = "totp">
                            <li id="kc-totp-period" class="mdc-typography--body1">${msg("loginTotpInterval")}: ${totp.policy.period}</li>
                        <#elseif totp.policy.type = "hotp">
                            <li id="kc-totp-counter" class="mdc-typography--body1">${msg("loginTotpCounter")}: ${totp.policy.initialCounter}</li>
                        </#if>
                    </ul>
                </p>
            </li>
        <#else>
            <li>
                <p class="mdc-typography--body1">${msg("loginTotpStep2")}</p>
                <img id="kc-totp-secret-qr-code" src="data:image/png;base64, ${totp.totpSecretQrCode}" alt="Figure: Barcode"><br/>
                <p><a href="${totp.manualUrl}" id="mode-manual" class="mdc-typography--body1">${msg("loginTotpUnableToScan")}</a></p>
            </li>
        </#if>
        <li>
            <p class="mdc-typography--body1">${msg("loginTotpStep3")}</p>
        </li>
    </ol>
    <form action="${url.loginAction}" class="form config-totp ${properties.kcFormClass!}" id="kc-totp-settings-form" method="post">
        
        <div class="mdc-layout-grid">
            <div class="mdc-layout-grid__inner">
                <div class="mdc-layout-grid__cell--span-6">
                    <div class="row ${properties.kcFormGroupClass!}">
            <div style="width:140px" class="mdc-text-field mdc-text-field--outlined mdc-text-field--with-leading-icon ${properties.kcLabelClass!}">
                <i class="material-icons mdc-text-field__icon" tabindex="-1" role="button">lock</i>
                <input id="totp" autocomplete="off" class="${properties.kcInputClass!}" name="totp" type="text">
                <div class="mdc-notched-outline">
                    <div class="mdc-notched-outline__leading"></div>
                    <div class="mdc-notched-outline__notch">
                    </div>
                    <div class="mdc-notched-outline__trailing"></div>
                </div>
            </div>
            <input type="hidden" id="totpSecret" name="totpSecret" value="${totp.totpSecret}" />
        </div>
                </div>
                <div class="mdc-layout-grid__cell--span-6">
                    <div class="row config-totp-button-container">
                        <input class="${properties.kcButtonClass!} ${properties.kcButtonPrimaryClass!} ${properties.kcButtonLargeClass!}" type="submit" value="${msg("doSubmit")}"/>
                    </div>
                </div>
                
            </div>
        </div>
    </form>
    </#if>
</@layout.registrationLayout>

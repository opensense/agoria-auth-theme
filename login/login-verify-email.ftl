<#import "template.ftl" as layout>
<@layout.registrationLayout; section>
    <#if section = "header">
        ${msg("emailVerifyTitle")}
    <#elseif section = "form">
        <p class="instruction mdc-typography--body1">
            ${msg("emailVerifyInstruction1")}
        </p>
        <p class="instruction mdc-typography--body1">
            ${msg("emailVerifyInstruction2")} <a href="${url.loginAction}" class="mdc-typography--body1">${msg("doClickHere")}</a> ${msg("emailVerifyInstruction3")}
        </p>
    </#if>
</@layout.registrationLayout>